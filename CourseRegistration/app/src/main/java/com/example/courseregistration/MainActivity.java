package com.example.courseregistration;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("BSc. Computer Science");
        arrayList.add("BSc. Applied Computer Science");
        arrayList.add("BSc. IT");
        arrayList.add("Diploma in Computer Science");
        arrayList.add("MSc. Computer Science");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        Button button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Registered.class);
                String program = spinner.getSelectedItem().toString();

                if(program.length() == 0){
                    Toast.makeText(getApplicationContext(), "Select a Program", Toast.LENGTH_LONG).show();
                }else{
                    intent.putExtra("Program",program);
                    startActivity(intent);
                }

            }
        });
    }
}
